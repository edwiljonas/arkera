## Available Scripts

In the project directory, you can run:

### `npm run install`

Install all the required dependencies.

### `npm run start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `Search Criteria`

http://localhost:3000/city <br>
http://localhost:3000/country <br>
http://localhost:3000/allBuildings <br>
http://localhost:3000/100m <br>
http://localhost:3000/150m <br>
http://localhost:3000/200m <br>
http://localhost:3000/300m <br>
http://localhost:3000/telecomTowers <br>
http://localhost:3000/allStructures

When searching with the criteria above, you will see the column highlighted in steelblue.

#### `Ive also added in redux, I created an initial state of the cities and countries.`