import React, {Component, Fragment} from 'react';

class Tags extends Component {

  render() {

    return (
      <Fragment>
        <div className="container">
          <div className="tag-title">Search Criteria:</div>
          <div className="tag">city</div>
          <div className="tag">country</div>
          <div className="tag">allBuildings</div>
          <div className="tag">100m</div>
          <div className="tag">150m</div>
          <div className="tag">200m</div>
          <div className="tag">300m</div>
          <div className="tag">telecomTowers</div>
          <div className="tag">allStructures</div>
        </div>
      </Fragment>
    );
  }

}

export default Tags;