import React, {Component, Fragment} from 'react';
import connect from "react-redux/es/connect/connect";
import {fetchCities} from "../../actions/baseActions";
import Tags from "../tags";

class Cities extends Component {

  constructor(props){
    super(props);
    this.state = {
      order: 'ASC',
      cities: []
    };
    this.massageData = this.massageData.bind(this);
  }

  componentWillMount() {
    this.props.fetchCities();
    this.massageData(this.props.column);
  }

  massageData(column){

    let data;
    if(column === ('city' || 'country')){
      data = this.props.cities.sort(
        (a,b) => a[column].localeCompare(b[column])
      );
    } else {
      data = this.props.cities.sort(
        (a, b) => parseInt(b[column]) - parseInt(a[column])
      );
    }
    this.setState({
        cities: data
    });

  }

  render() {

      const cities = this.state.cities.map( (city, index) => (
          <div className="full-row list-row" key={"unique-city-"+index}>
            <div className={'col-2 ' + (this.props.column === "city" ? 'searched' : '')}>{city["city"]}</div>
            <div className={'col-2 ' + (this.props.column === "country" ? 'searched' : '')}>{city["country"]}</div>
            <div className={'col-2 ' + (this.props.column === "allBuildings" ? 'searched' : '')}>{city["allBuildings"]}</div>
            <div className={'col-1 ' + (this.props.column === "100m" ? 'searched' : '')}>{city["100m"]}</div>
            <div className={'col-1 ' + (this.props.column === "150m" ? 'searched' : '')}>{city["150m"]}</div>
            <div className={'col-1 ' + (this.props.column === "200m" ? 'searched' : '')}>{city["200m"]}</div>
            <div className={'col-1 ' + (this.props.column === "300m" ? 'searched' : '')}>{city["300m"]}</div>
            <div className={'col-1 ' + (this.props.column === "telecomTowers" ? 'searched' : '')}>{city["telecomTowers"]}</div>
            <div className={'col-1 ' + (this.props.column === "allStructures" ? 'searched' : '')}>{city["allStructures"]}</div>
          </div>
      ));

    return (
      <Fragment>
        <Tags/>
        <div className="container">
          <div className="full-row list-header">
            <div className="col-2">City</div>
            <div className="col-2">Country</div>
            <div className="col-2">All Buildings</div>
            <div className="col-1">100m</div>
            <div className="col-1">150m</div>
            <div className="col-1">200m</div>
            <div className="col-1">300m</div>
            <div className="col-1">Towers</div>
            <div className="col-1">All</div>
          </div>
        </div>
        <div className="container">
          {cities}
        </div>
      </Fragment>
    );
  }

}

const mapStateToProps = state => ({
  cities: state.data.cities,
});

export default connect(mapStateToProps, { fetchCities })(Cities);