import React, {Component, Fragment} from 'react';
import Cities from "../../components/cities";

class Home extends Component {

  render() {
    return (
      <Fragment>
        <Cities
          column={this.props.match.params.column}
        />
      </Fragment>
    );
  }

}

export default Home;