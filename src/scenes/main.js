import React, { Component } from 'react';
import Home from "./home";
import { BrowserRouter as Router, Route } from "react-router-dom";

class Main extends Component {
  render() {
    return (
      <Router>
        <div>
          <Route exact path={'/'} component={Home} />
          <Route path={'/:column'} component={Home} />
        </div>
      </Router>
    );
  }
}

export default Main;
