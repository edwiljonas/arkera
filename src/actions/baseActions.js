import { GET_CITIES } from "../actions/types";

export const fetchCities = () => dispatch => {
  dispatch({
    type: GET_CITIES
  });
};